import express from "express"
import route from "./routes/index.js"


const app = express()
const port = 3000

app.use(express.json())
app.use(route)
app.use('/assets', express.static('assets'));

app.listen(port, () => {
  console.log(`Server running at port ${port}`)
})