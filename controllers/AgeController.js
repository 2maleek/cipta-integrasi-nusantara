
export const calculateage = async (req, res) => {
  try {
    let { dateofbirth } = req.params
    
    dateofbirth = dateofbirth.split('-')
    const now = new Date("10-03-2022") // REST API dipanggil pada tanggal format mm-dd-yyyy
    let year = 0, month = 0, day = 0, date

    let birthDay = parseInt(dateofbirth[0])
    let birthMonth = parseInt(dateofbirth[1])
    let birthYear = parseInt(dateofbirth[2])

    let nowYear = now.getFullYear()
    let nowMonth = now.getMonth() + 1
    let nowDay = now.getDay()

    if(nowDay < birthDay){
      date = new Date(nowYear, nowMonth, 0)
      day = nowDay - birthDay + date.getDate() + 12 - birthMonth
      month--
    }else{
      day = nowDay - birthDay 
    }

    if(nowMonth < birthMonth || month < 0){
      year = nowYear - birthYear - 1
      month += nowMonth - birthMonth + 12
    }else{
      year = nowYear - birthYear
      month = nowMonth - birthMonth
    }
    
    return res.status(200).send({
      "umur": {
        "year": year,
        "month": month,
        "day": day,
      }
    });

  } catch (error) {
      res.status(500).json({message: error.message});
  }
   
}