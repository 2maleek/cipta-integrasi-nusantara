export const validasialergiobat = async (req, res) => {
  try {
    const listObat = [
      {
        obat: "Proris",
        kandungan: "Ibuprofen 250mg",
      },
      {
        obat: "Paratusin",
        kandungan:
          " paracetamol125 mg, pseudoepedrid 7.5 mg, noscapine 10 mg, ctm 0.5 mg, guafenisin 25 mg, succus liquiritae 125 ethanol 10 %",
      },
      {
        obat: "CARDIO ASPIRIN ",
        kandungan:
          " mengandung Acetylsalicylic Acid 80 mg. Acetylsalicylic acid atau dikenal juga dengan Aspirin merupakan senyawa analgesik non steroid yang digunakan sebagai analgesik, antipiretik, antiinflamasi dan anti-platelet.",
      },
    ];

    const { pasien, resep } = req.body;
    let result = [];
    resep.forEach((itemResep) => {
      listObat.forEach((itemObat) => {
        // search nama obat yang sama
        if (itemObat.obat.toLowerCase() === itemResep.obat.toLowerCase()) {
          // replace text, jika tidak berubah maka tidak mengandung kata yang dicari
          const testEdit = itemObat.kandungan
            .toLowerCase()
            .replace(
              pasien.alergi.toLowerCase(),
              "Kalo tidak match berarti mengandung kata yang dicari"
            );
          if (itemObat.kandungan.toLowerCase() !== testEdit) {
            result.push({ obat: itemObat.obat });
          }

          // // quick trick js 1
          // if(itemObat.kandungan.toLowerCase().split(pasien.alergi.toLowerCase()).length > 1){
          //   result.push({ obat: itemObat.obat })
          // }

          // // quick trick js 2
          // if(itemObat.kandungan.toLowerCase().includes(pasien.alergi.toLowerCase())){
          //   result.push({ obat: itemObat.obat })
          // }

          // // quick trick js 3
          // if(itemObat.kandungan.toLowerCase().indexOf(pasien.alergi.toLowerCase()) >= 0){
          //   result.push({ obat: itemObat.obat })
          // }
        }
      });
    });

    return res.status(200).send({
      resep: result,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
