export const hitunggaji = async (req, res) => {
  try {
    const rules = [
      {
        country: "indonesia",
        ptkp: [
          {
            is_married: false,
            is_has_child: false,
            ptkp: 25,
          },
          {
            is_married: true,
            is_has_child: false,
            ptkp: 50,
          },
          {
            is_married: true,
            is_has_child: true,
            ptkp: 75,
          },
        ],
        pajak_tahunan: [
          {
            min: 0,
            max: 50,
            is_calculate: true,
            percentage: 5,
          },
          {
            min: 50,
            max: 250,
            is_calculate: true,
            percentage: 10,
          },
          {
            min: 250,
            max: null,
            is_calculate: true,
            percentage: 15,
          },
        ],
      },
      {
        country: "vietnam",
        ptkp: [
          {
            is_married: false,
            is_has_child: null,
            ptkp: 15,
          },
          {
            is_married: true,
            is_has_child: null,
            ptkp: 30,
          },
        ],
        pajak_tahunan: [
          {
            min: 0,
            max: 50,
            is_calculate: false,
            percentage: 2.5,
          },
          {
            min: 50,
            max: null,
            is_calculate: false,
            percentage: 7.5,
          },
        ],
      },
    ];

    const { employee, komponengaji } = req.body;
    let semua_penghasilan = 0;
    let penghasilan_netto = 0;
    let semua_asuransi = 0;
    let PTKP = 0;
    let total_pajak_setahun = 0;
    let total_pajak_bulan_ini = 0;
    let layer = [];

    komponengaji.forEach((item) => {
      semua_penghasilan += (item.penghasilan_sebulan * 12) / 1000000;
      semua_asuransi += ((item?.asuransi || 0) * 12) / 1000000;
    });

    rules.forEach((rule) => {
      if (rule.country === employee.country) {
        rule.ptkp.forEach((item) => {
          if (
            (item.is_has_child === null ||
              item.is_has_child === employee.is_has_child) &&
            item.is_married === employee.is_married
          ) {
            PTKP = item.ptkp;
            layer = rule.pajak_tahunan;
          }
        });
      }
    });

    penghasilan_netto += semua_penghasilan - semua_asuransi - PTKP;

    let calculatedPercentage = 0;
    layer.forEach((item) => {
      calculatedPercentage += item.percentage;
      if (item.is_calculate) {
      } else {
        calculatedPercentage = item.percentage;
      }

      if (item.max !== null && penghasilan_netto - item.max >= 0) {
        total_pajak_setahun += (item.max * calculatedPercentage) / 100;
        console.log(
          "calculatedPercentage",
          (item.max * calculatedPercentage) / 100
        );

        penghasilan_netto -= item.max;
      } else if (item.max !== null && penghasilan_netto - item.max < 0) {
        total_pajak_setahun += (penghasilan_netto * calculatedPercentage) / 100;
        console.log(
          "calculatedPercentage",
          (penghasilan_netto * calculatedPercentage) / 100
        );

        penghasilan_netto -= item.max;
      } else if (penghasilan_netto > 0) {
        total_pajak_setahun += (penghasilan_netto * calculatedPercentage) / 100;
        console.log(
          "calculatedPercentage",
          (penghasilan_netto * calculatedPercentage) / 100
        );

        penghasilan_netto = 0;
      }
    });

    total_pajak_bulan_ini = (total_pajak_setahun / 12).toFixed(3);
    return res.status(200).send(`${total_pajak_bulan_ini * 1000000}`);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
