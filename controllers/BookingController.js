import fs from "fs"
import moment from "moment";

export const bookingKamarOperasi = async (req, res) => {
  try {
      // bookingdate format(dd-mm-yyyy_hh:mm)
      const { bookingdate, durasi } = req.params
      const start_time = moment(bookingdate, 'DD-MM-YYYY_HH:mm').format('YYYY-MM-DD HH:mm')
      const time = moment(bookingdate, 'DD-MM-YYYY_HH:mm').format('HH:mm')
      let end_time = moment(start_time).add(durasi, 'hour').format('YYYY-MM-DD HH:mm')
      let after_break = moment(end_time).add(2, 'hour').format('YYYY-MM-DD HH:mm')

      let unfilteredListBooking = JSON.parse(fs.readFileSync("assets/listBooking.json", 'utf-8'))
      let listBooking = unfilteredListBooking

      listBooking = listBooking.sort((a,b) => (a.end_time > b.end_time) ? 1 : ((b.end_time > a.end_time) ? -1 : 0))
      let isAvailable = true

      // Method 1
      listBooking = listBooking.filter((item) => {
        return moment(item.end_time).isSame(start_time, 'date') && 
          ( moment(item.end_time).add(2, 'hour').diff(moment(start_time), 'second') > 0
          && moment(after_break).diff(moment(item.start_time), 'second') > 0 )

      })
      isAvailable = listBooking.length > 0 ? false : true
      // End Method 1

      
      // // Method 2
      // listBooking.forEach(item => {
      //   if( moment(item.end_time).isSame(start_time, 'date') && 
      //       ( moment(item.end_time).add(2, 'hour').diff(moment(start_time), 'second') > 0
      //         && moment(after_break).diff(moment(item.start_time), 'second') > 0 
      //       )
      //   ){
      //     isAvailable = false
      //   }
      // });
      // // End Method 2

      if( !isAvailable ){
        return res.status(200).send(false);
      }else{
        unfilteredListBooking.push({
          start_time,
          end_time,
          durasi,
        })
        fs.writeFileSync("assets/listBooking.json", JSON.stringify(unfilteredListBooking, null, 2))
        return res.status(200).send(true);
      }
  } catch (error) {
      res.status(500).json({message: error.message});
  }
   
}