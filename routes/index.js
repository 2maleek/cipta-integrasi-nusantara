import express from "express";
import { bookingKamarOperasi } from "../controllers/BookingController.js"
import { hitunggaji } from "../controllers/PenggajianController.js"
import { validasialergiobat } from "../controllers/ResepController.js"
import { calculateage } from "../controllers/AgeController.js"

const router = express.Router()

router.get('/bookingkamaroperasi/:bookingdate/:durasi', bookingKamarOperasi)
router.post('/hitunggaji', hitunggaji)
router.post('/validasialergiobat', validasialergiobat)
router.get('/calculateage/:dateofbirth', calculateage)

export default router